import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { Http} from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {
  private _url = '/api'
  private _url2 = '/assets/contacts.json'
  constructor(
    private _http:HttpClient
  ) { }

  getAll(){
    return this._http.get(this._url2)
    .map((data:any)=>{
      // console.log(data)
      return data.contacts
    })
  }

}
