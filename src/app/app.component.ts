import { Component, OnInit, Inject } from '@angular/core';
import { DataService } from './data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { element } from 'protractor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  addEntryForm: FormGroup
  searchStr: string

  private ids: Array<number>
  public contacts: Array<any>


  constructor(
    @Inject(FormBuilder) fb: FormBuilder,
    private _dataService: DataService
  ) {
    this.addEntryForm = fb.group({
      name: ['', Validators.required],
      phone_number: ['', Validators.required],
      address: ['']
    })

    this.searchStr = ''

  }


  ngOnInit() {
    this._dataService.getAll()
      .subscribe((res: any) => {
        if (res) {
          res.forEach(element => {
            element.show = true
          });
          this.contacts = res;
        }
      })
  }

  addEntry() {
    let newEntry: any = {
      show: true,
      name: this.addEntryForm.value.name,
      phone_number: this.addEntryForm.value.phone_number,
      address: this.addEntryForm.value.address
    }
    this.contacts.push(newEntry)
    this.addEntryForm.reset()
  }

  editEntry(contact: any) {

    let index = this.findIndex(contact, this.contacts)
    this.contacts[index] = contact


  }



  deleteEntry(contact: any) {
    let index = this.findIndex(contact, this.contacts)
    this.contacts.splice(index, 1)
  }


  searchContacts() {

    if (this.searchStr.length > 0) {
      this.contacts.forEach((item) => {

        let name = item.name.toLowerCase();
        let phone = item.phone_number;

        if (name.indexOf(this.searchStr.toLowerCase()) > -1 || phone.indexOf(this.searchStr) > -1) {
          item.show = true
        } else {
          item.show = false
        }
      });
    } else {
      this.contacts.forEach((item) => {
        item.show = true
      })
    }

  }


  findIndex(obj, arr) {
    if (arr.length) {
      return arr.indexOf(obj)
    } else {
      alert('Problem with index of contacts')
    }
    return
  }

  sortContacts(order) {
    this.contacts.sort((a: any, b: any) => {
      let prev = a.name.toUpperCase()
      let next = b.name.toUpperCase()

      if (prev < next) {
        let val1
        order === 'asc' ? val1 = -1 : val1 = 1
        return val1
      }
      if (prev > next) {
        let val2
        order === 'asc' ? val2 = 1 : val2 = -1
        return val2
      }
      return 0;
    })
  }







}
